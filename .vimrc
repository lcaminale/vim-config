set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
set laststatus=2

" Change how vim represents characters on the screen
set encoding=utf-8

" Set the encoding of files written
set fileencoding=utf-8

set number 

" Menu bar color scheme : https://github.com/itchyny/lightline.vim
let g:lightline = {
      \ 'colorscheme': 'jellybeans',
      \ }

" Enavle jedi 'autocompletion + function implementation jump' 
let g:jedi#auto_initialization = 1
let g:jedi#auto_vim_configuration = 1

" Terraform plugin 
let g:terraform_align=1
let g:terraform_fmt_on_save=1
let g:terraform_fold_sections=1
"autocmd BufEnter *.hcl* colorscheme nord
" GOLANG - setup
let g:go_fmt_command = "goimports"    " Run goimports along gofmt on each save     
let g:go_auto_type_info = 1           " Automatically get signature/type info for object under cursor     
au filetype go inoremap <buffer> . .<C-x><C-o>

" air-line plugin specific commands
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
"
call vundle#begin()
" Programming languages setup
Plugin 'fatih/vim-go'

" Colorscheme theme
Plugin 'blueshirts/darcula'
Plugin 'arcticicestudio/nord-vim', { 'for': 'go' }
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Git plugin: use with :Git --> https://github.com/tpope/vim-fugitive
Plugin 'tpope/vim-fugitive'

" File system explorer 
Plugin 'scrooloose/nerdtree'

" Docker plugin
Plugin 'kkvh/vim-docker-tools'

" A light and configurable statusline/tabline plugin for Vim
Plugin 'itchyny/lightline.vim'

" Terraform plugin
Plugin 'hashivim/vim-terraform'
Plugin 'neoclide/coc.nvim', {'branch': 'release'} " https://dev.to/braybaut/integrate-terraform-language-server-protocol-with-vim-38g

" Yaml color
Plugin 'stephpy/vim-yaml'

" Multi cursor
Plugin 'terryma/vim-multiple-cursors'

" Autocompletion + find function definition
Plugin 'davidhalter/jedi-vim'

" Fuzzy finder
Plugin 'junegunn/fzf.vim'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }

" Python color scheme
Plugin 'vim-python/python-syntax'

" Linter
Plugin  'w0rp/ale'

" HCL plugin
Plugin 'yorinasub17/vim-terragrunt'

" All of your Plugins must be added before the following line
call vundle#end()            " required

set termguicolors
syntax enable
colorscheme darcula

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

